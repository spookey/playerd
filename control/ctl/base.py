from logging import Logger, getLogger
from typing import Final, Generic, Optional

from control.lib.core import T
from control.lib.disk import dump_yaml, load_yaml, normalize


class YBase(Generic[T]):
    def __init__(self, location: str, fallback: T) -> None:
        self.log: Final[Logger] = getLogger(self.__class__.__name__)

        self.location: Final[str] = normalize(location)
        self._fallback: Final[T] = fallback
        self._content: Optional[T] = None

        self.log.debug(
            '%s initialized for "%s"',
            self.__class__.__name__,
            self.location,
        )

    def __repr__(self) -> str:
        name: Final[str] = self.__class__.__name__
        return f"{name}('{len(self.content)}')"

    @property
    def content(self) -> T:
        if self._content is None:
            self._content = load_yaml(self.location, self._fallback)
        return self._content

    def save(self) -> bool:
        return dump_yaml(self.location, self.content)
