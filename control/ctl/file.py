from os import path
from typing import Final, Union

from control.lib.disk import normalize


class File:
    def __init__(self, location: str) -> None:
        self.location: Final[str] = normalize(location)

    def __repr__(self) -> str:
        name: Final[str] = self.__class__.__name__
        return f"{name}('{self.location}')"

    def __hash__(self) -> int:
        return hash(self.location)

    def __eq__(self, other: Union["File", object]) -> bool:
        if not isinstance(other, File):
            return False
        return self.location == other.location

    def __lt__(self, other: Union["File", object]) -> bool:
        if not isinstance(other, File):
            return False
        return self.location < other.location

    def related(self, other: Union["File", object]) -> bool:
        if not isinstance(other, File):
            return False
        return self.trail == other.trail

    @property
    def present(self) -> bool:
        return path.exists(self.location)

    @property
    def trail(self) -> str:
        return path.dirname(self.location)
