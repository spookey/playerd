from typing import Final, Set

from control.ctl.base import YBase
from control.ctl.file import File
from control.lib.core import TSrcs
from control.lib.disk import discover, normalize


class Sources(YBase[TSrcs]):
    def add(self, *locations: str) -> bool:
        if not locations:
            self.log.error("location missing in options")
            return False

        bucket: Final[Set[str]] = set()
        for location in (normalize(loc) for loc in locations):
            if location not in bucket and location not in self.content:
                self.log.info('adding "%s" to sources', location)
                bucket.add(location)
        if bucket:
            bucket.update(self.content)
            self.content.clear()
            self.content.extend(sorted(normalize(loc) for loc in bucket))
        return True

    def scan(self) -> Set[File]:
        self.log.debug("scanning for sources...")
        result: Final[Set[File]] = set()
        for location in self.content:
            for element in discover(location, skip_links=False):
                result.add(File(element))

        self.log.info('discovered "%d" source files', len(result))
        return result
