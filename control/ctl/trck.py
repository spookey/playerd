from datetime import datetime
from random import choice
from typing import Final, Iterator, List, Optional, Tuple

from control.ctl.base import YBase
from control.ctl.file import File
from control.lib.core import TTrck
from control.lib.disk import normalize

ELEMS_LAST: Final[int] = 15
TABLE_DAYS: Final[int] = 7
TABLE_HOUR: Final[int] = 24
TABLE_SLOT: Final[float] = 0.25


class Tracker(YBase[TTrck]):
    def _prepare(self) -> None:
        def default_hours() -> Iterator[float]:
            num: float = 0.0
            while num < TABLE_HOUR:
                yield float(num)
                num += TABLE_SLOT

        self.content["last"] = self.content.get("last", {})
        self.content["sources"] = self.content.get("sources", {})
        self.content["timetable"] = self.content.get("timetable", {})

        d_hours: Final[List[float]] = list(default_hours())
        for day in range(1, 1 + TABLE_DAYS):
            self.content["timetable"][day] = sorted(
                set(self.content["timetable"].get(day, d_hours))
            )

    @staticmethod
    def _time_tup(stamp: Optional[datetime] = None) -> Tuple[int, float]:
        if not stamp:
            stamp = datetime.now()
        return (
            stamp.date().isoweekday(),
            stamp.hour + (stamp.minute / 60),
        )

    def set_current(self, current: File) -> None:
        self._prepare()

        backlog: Final[List[str]] = [current.location]
        backlog.extend(self.content["last"].get("list", []))
        self.content["last"]["list"] = backlog[:ELEMS_LAST]
        self.content["last"]["time"] = datetime.now()

    def drop_current(self) -> None:
        self._prepare()

        self.content["last"] = {}

    @property
    def last_files(self) -> List[File]:
        self._prepare()

        return [File(loc) for loc in self.content["last"].get("list", [])]

    @property
    def last_time(self) -> Tuple[int, float]:
        self._prepare()

        stamp: Final[datetime] = self.content["last"].get(
            "time", datetime.now()
        )
        return self._time_tup(stamp)

    @property
    def switched(self) -> bool:
        l_day, l_hour = self.last_time
        c_day, c_hour = self._time_tup()
        if l_day != c_day:
            return True
        table: Final[List[float]] = sorted(
            set(self.content["timetable"].get(c_day, []))
        )
        l_tbl: Final[List[float]] = [el for el in table if el <= l_hour]
        c_tbl: Final[List[float]] = [el for el in table if el <= c_hour]
        l_len: Final[int] = len(l_tbl)
        c_len: Final[int] = len(c_tbl)
        self.log.debug("last(%d) %s\ncurr(%d) %s", l_len, l_tbl, c_len, c_tbl)
        return l_len != c_len

    def set_files(self, src_loc: str, *files: File) -> None:
        src_loc = normalize(src_loc)
        self._prepare()

        self.content["sources"][src_loc] = sorted(
            file.location for file in files if file.present
        )

    def get_all_files(
        self, src_loc: str, spare_last: bool = False
    ) -> List[File]:
        self._prepare()

        files: Final[List[File]] = [
            file
            for file in (
                File(loc) for loc in self.content["sources"].get(src_loc, [])
            )
            if file.present
        ]

        if spare_last:
            spared: Final[List[File]] = [
                file for file in files if file not in self.last_files
            ]
            if spared:
                return spared

            self.log.info("too many previous files, returning all instead")
        return files

    def get_related_files(self, src_loc: str) -> List[File]:
        files: Final[List[File]] = self.get_all_files(src_loc, spare_last=True)
        backlog: Final[List[File]] = self.last_files

        if backlog:
            return [file for file in files if file.related(backlog[0])]

        self.log.debug("no previous files, returning all instead")
        return files

    def get_suggested_files(self, src_loc: str) -> List[File]:
        if not self.switched:
            files: Final[List[File]] = self.get_related_files(src_loc)
            if files:
                return files
        return self.get_all_files(src_loc, spare_last=True)

    def suggest(self, src_loc: str) -> Optional[File]:
        files: Final[List[File]] = self.get_suggested_files(src_loc)
        if files:
            return choice(files)
        self.log.warning("nothing to suggest")
        return None
