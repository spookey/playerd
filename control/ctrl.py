from logging import Logger, getLogger
from sys import stdout
from typing import Final, Optional

from control.ctl.file import File
from control.ctl.srcs import Sources
from control.ctl.trck import Tracker
from control.lib.core import Args


class Control:
    def __init__(self, args: Args) -> None:
        self.log: Final[Logger] = getLogger(self.__class__.__name__)

        self.sources: Final[Sources] = Sources(args.sources, [])
        self.tracker: Final[Tracker] = Tracker(args.tracker, {})

        self.log.debug("%s initialized", self.__class__.__name__)

    def __repr__(self) -> str:
        name = self.__class__.__name__
        return f"{name}('{self.sources.location}', '{self.tracker.location}')"

    def do_add(self, *options: str) -> bool:
        if self.sources.add(*options):
            return self.sources.save()
        return False

    def do_scan(self, *_: str) -> bool:
        self.tracker.set_files(
            self.sources.location,
            *self.sources.scan(),
        )

        return self.tracker.save()

    def do_clear(self, *_: str) -> bool:
        self.tracker.drop_current()
        return self.tracker.save()

    def do_suggest(self, *_: str) -> bool:
        file: Final[Optional[File]] = self.tracker.suggest(
            self.sources.location
        )
        if not file:
            return False

        location: Final[str] = file.location.replace("'", "\\'")
        stdout.write(f"'{location}'\n")
        stdout.flush()

        self.tracker.set_current(file)
        return self.tracker.save()
