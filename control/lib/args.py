from argparse import REMAINDER, ArgumentParser
from typing import cast

from control.lib.core import Args
from control.lib.note import LOG_LEVELS
from control.lib.refl import ctrl_actions


def arguments() -> Args:
    def _help(text: str) -> str:
        return f"{text} (default: '%(default)s')"

    parser = ArgumentParser(
        "pctl",
        epilog="c[_]",
        add_help=True,
        allow_abbrev=True,
    )

    parser.add_argument(
        "--verbosity",
        dest="verbosity",
        choices=LOG_LEVELS,
        default="warning",
        help=_help("logging level"),
    )

    parser.add_argument(
        "--tracker",
        dest="tracker",
        default="tracker.yaml",
        help=_help("tracker file"),
    )
    parser.add_argument(
        "--sources",
        dest="sources",
        default="sources.yaml",
        help=_help("sources file"),
    )

    parser.add_argument(
        "action",
        choices=ctrl_actions(),
        help="run specified action",
    )
    parser.add_argument(
        "options",
        nargs=REMAINDER,
        help="additional options for some of the actions",
    )

    return cast(Args, parser.parse_args())
