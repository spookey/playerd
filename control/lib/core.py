from datetime import datetime
from typing import Collection, Dict, List, NamedTuple, TypedDict, TypeVar


class Args(NamedTuple):
    verbosity: str
    tracker: str
    sources: str
    action: str
    options: Collection[str]


TSrcs = List[str]

_last = TypedDict("_last", {"list": TSrcs, "time": datetime}, total=False)
TTrck = TypedDict(
    "TTrck",
    {
        "last": _last,
        "sources": Dict[str, TSrcs],
        "timetable": Dict[int, List[float]],
    },
    total=False,
)

T = TypeVar("T", TSrcs, TTrck)
