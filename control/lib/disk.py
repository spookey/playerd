from logging import Logger, getLogger
from os import path, walk
from typing import Collection, Final, Iterator, Optional

from yaml import YAMLError, safe_dump, safe_load

from control.lib.core import T

ENCODING: Final[str] = "utf-8"
EXTENSIONS: Final[Collection[str]] = (
    "avi",
    "m4v",
    "mkv",
    "mov",
    "mp4",
    "mpeg",
    "mpg",
    "ogv",
    "webm",
)

LOG: Final[Logger] = getLogger(__name__)


def normalize(location: str) -> str:
    return path.abspath(path.expandvars(path.expanduser(location)))


def extension(location: str) -> str:
    _, ext = path.splitext(location)
    return ext.lstrip(path.extsep).lower()


def load_yaml(location: str, fallback: T) -> T:
    location = normalize(location)
    LOG.debug('loading yaml from "%s"', location)
    if path.exists(location) and path.isfile(location):
        with open(location, "r", encoding=ENCODING) as handle:
            content: Optional[T] = None
            try:
                content = safe_load(stream=handle)
            except YAMLError as ex:
                LOG.exception(ex)
                LOG.error('cannot read yaml file "%s"', location)
            if content is not None and isinstance(content, type(fallback)):
                return content
    LOG.info('returning fallback for "%s"', location)
    return fallback


def dump_yaml(location: str, content: T) -> bool:
    location = normalize(location)
    LOG.debug('dumping yaml to "%s"', location)
    with open(location, "w", encoding=ENCODING) as handle:
        try:
            safe_dump(
                content,
                stream=handle,
                allow_unicode=True,
                canonical=False,
                default_flow_style=False,
                explicit_end=False,
                explicit_start=False,
                indent=2,
            )
            return True
        except YAMLError as ex:
            LOG.exception(ex)
            LOG.error('cannot write yaml file "%s"', location)
    return False


def discover(location: str, skip_links: bool = True) -> Iterator[str]:
    location = normalize(location)

    def match(loc: str) -> bool:
        if skip_links and path.islink(loc):
            return False
        return extension(loc) in EXTENSIONS

    LOG.debug('walking tree at "%s"', location)
    if path.exists(location):
        if path.isfile(location):
            if match(location):
                yield location
        else:
            for directory, _, files in walk(location):
                for file_name in files:
                    result = normalize(path.join(directory, file_name))
                    if match(result):
                        yield result
