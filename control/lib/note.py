from logging import (
    DEBUG,
    ERROR,
    INFO,
    WARNING,
    Formatter,
    Handler,
    Logger,
    StreamHandler,
    getLogger,
)
from typing import Final, Mapping

LOG_LEVELS: Final[Mapping[str, int]] = {
    "debug": DEBUG,
    "error": ERROR,
    "info": INFO,
    "warn": WARNING,
    "warning": WARNING,
}


def log_setup(verbosity: str) -> None:
    root_log: Final[Logger] = getLogger()
    formatter: Final[Formatter] = Formatter(
        """
%(levelname)s — %(asctime)s | %(name)s
%(module)s.%(funcName)s [ %(pathname)s:%(lineno)d ]

%(message)s

----
    """.strip()
    )
    handler: Final[Handler] = StreamHandler(stream=None)

    level_dbg: Final[int] = LOG_LEVELS["debug"]
    level_use: Final[int] = LOG_LEVELS.get(verbosity, level_dbg)

    handler.setFormatter(formatter)
    handler.setLevel(level_use)
    root_log.setLevel(level_dbg)
    root_log.addHandler(handler)
