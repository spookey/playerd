from logging import Logger, getLogger
from typing import Callable, Final, Mapping, Optional, Sequence

from control.ctrl import Control

LOG: Final[Logger] = getLogger(__name__)
PREFIX: Final[str] = "do_"

R = Callable[[Control, Sequence[str]], bool]


def ctrl_actions() -> Mapping[str, R]:
    return {
        name.split(PREFIX)[-1]: func
        for name, func in vars(Control).items()
        if name.startswith(PREFIX)
    }


def ctrl_router(ctrl: Control, action: str, *options: str) -> Optional[bool]:
    actions: Final[Mapping[str, R]] = ctrl_actions()
    func: Final[Optional[R]] = actions.get(action, None)

    if func:
        return func(ctrl, *options)

    LOG.warning('action "%s" not found', action)
    return None
