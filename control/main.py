from logging import Logger, getLogger
from typing import Final

from control.ctrl import Control
from control.lib.args import arguments
from control.lib.core import Args
from control.lib.note import log_setup
from control.lib.refl import ctrl_router

LOG: Final[Logger] = getLogger(__name__)


def run() -> int:
    args: Final[Args] = arguments()
    log_setup(args.verbosity)
    ctrl: Final[Control] = Control(args)

    if not ctrl_router(ctrl, args.action, *args.options):
        LOG.error("something went wrong.")
        return 1
    return 0
