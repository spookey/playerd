VER_PY			:=	3.11
DIR_VENV		:=	venv

CMD_BLACK		:=	$(DIR_VENV)/bin/black
CMD_ISORT		:=	$(DIR_VENV)/bin/isort
CMD_MYPY		:=	$(DIR_VENV)/bin/mypy
CMD_PIP			:=	$(DIR_VENV)/bin/pip$(VER_PY)
CMD_PYLINT		:=	$(DIR_VENV)/bin/pylint
CMD_PYTHON3		:=	$(DIR_VENV)/bin/python$(VER_PY)
LIB_YAML		:=	$(DIR_VENV)/lib/python$(VER_PY)/site-packages/yaml/__init__.py

DIR_MOVIES		:=	~/Movies

CMD_MPLAYER		:=	mplayer
CMD_PLAYERD		:=	./playerd.sh
FFO_RECVING		:=	mp_recv.fifo
FFO_SENDING		:=	mp_send.fifo
FLE_SOURCES		:=	sources.yaml
FLE_TRACKER		:=	tracker.yaml
LOG_VERBOSE		:=	debug
SCR_CONTROL		:=	playerctl.py

SOURCES			:= \
					"control" \
					"$(SCR_CONTROL)" \


.PHONY: help
help:
	@echo "playerd makefile"
	@echo "————————————————"
	@echo
	@echo "add                  add $(DIR_MOVIES) to $(FLE_SOURCES)"
	@echo "scan                 scan for files defined in $(FLE_SOURCES)"
	@echo "clear                drop last played items from $(FLE_TRACKER)"
	@echo "suggest              (internal) get suggestion from $(FLE_TRACKER)"
	@echo "run                  launch using $(FLE_SOURCES) and $(FLE_TRACKER)"
	@echo
	@echo "— development —"
	@echo
	@echo "venv                 create virtualenv"
	@echo "requirements         install requirements int venv"
	@echo
	@echo "black                run black on code"
	@echo "isort                run isort on code"
	@echo "mypy                 run mypy on code"
	@echo "pylint               run pylint on code"
	@echo "action               run all development tasks"
	@echo


$(DIR_VENV):
	python$(VER_PY) -m venv "$(DIR_VENV)"
	$(CMD_PIP) install -U pip setuptools

$(LIB_YAML): $(DIR_VENV)
	$(CMD_PIP) install -r "requirements.txt"

$(CMD_BLACK) $(CMD_ISORT) $(CMD_MYPY) $(CMD_PYLINT): $(DIR_VENV)
	$(CMD_PIP) install -r "requirements-dev.txt"

.PHONY: requirements
requirements: $(LIB_YAML)
.PHONY: requirements-dev
requirements-dev: $(CMD_BLACK) $(CMD_ISORT) $(CMD_MYPY) $(CMD_PYLINT)


define _black
	$(CMD_BLACK) --line-length=79 $(1)
endef

.PHONY: black
black: requirements-dev
	$(call _black,$(SOURCES))


define _isort
	$(CMD_ISORT) --line-length=79 --profile=black $(1)
endef

.PHONY: isort
isort: requirements-dev
	$(call _isort,$(SOURCES))


define _pylint
	$(CMD_PYLINT) \
		--disable="missing-class-docstring" \
		--disable="missing-function-docstring" \
		--disable="missing-module-docstring" \
		--output-format="colorized" \
			$(1)
endef

.PHONY: pylint
pylint: requirements-dev
	$(call _pylint,$(SOURCES))


define _mypy
	$(CMD_MYPY) \
		--strict \
			$(1)
endef

.PHONY: mypy
mypy: requirements-dev
	$(call _mypy,$(SOURCES))


.PHONY: action
action: isort black mypy pylint


define _playctl
	$(CMD_PYTHON3) $(SCR_CONTROL) \
		--verbosity "$(LOG_VERBOSE)" \
		$(1)
endef
define _playerd
	CMD_MPLAYER="$(CMD_MPLAYER)" \
	CMD_PYTHON3="$(CMD_PYTHON3)" \
	FFO_RECVING="$(FFO_RECVING)" \
	FFO_SENDING="$(FFO_SENDING)" \
	FLE_SOURCES="$(FLE_SOURCES)" \
	FLE_TRACKER="$(FLE_TRACKER)" \
	LOG_VERBOSE="$(LOG_VERBOSE)" \
	SCR_CONTROL="$(SCR_CONTROL)" \
		$(CMD_PLAYERD) \
			$(1)
endef

.PHONY: add
add: requirements
	$(call _playctl,add "$(DIR_MOVIES)")

.PHONY: scan
scan: requirements
	$(call _playctl,scan)

.PHONY: clear
clear: requirements
	$(call _playctl,clear)

.PHONY: suggest
suggest: requirements
	$(call _playctl,suggest)

.PHONY: run
run: requirements
	$(call _playerd,)


.PHONY: kill
kill:
	pkill -x "$(CMD_MPLAYER)" || :
	pkill -f "$(CMD_PLAYERD)" || :
	rm "$(FFO_RECVING)" || :
	rm "$(FFO_SENDING)" || :
