from sys import exit as _exit

from control.main import run

if __name__ == "__main__":
    _exit(run())
