#!/usr/bin/env sh

DIR_PLAYERD=$(cd "$(dirname "$0")" || exit 1; pwd)

CMD_MPLAYER=${CMD_MPLAYER:-"mplayer"}
CMD_PYTHON3=${CMD_PYTHON3:-"$DIR_PLAYERD/venv/bin/python3"}
FFO_RECVING=${FFO_RECVING:-"$DIR_PLAYERD/mp_recv.fifo"}
FFO_SENDING=${FFO_SENDING:-"$DIR_PLAYERD/mp_send.fifo"}
FLE_SOURCES=${FLE_SOURCES:-"$DIR_PLAYERD/sources.yaml"}
FLE_TRACKER=${FLE_TRACKER:-"$DIR_PLAYERD/tracker.yaml"}
LOG_VERBOSE=${LOG_VERBOSE:-"info"}
SCR_CONTROL=${SCR_CONTROL:-"$DIR_PLAYERD/playerctl.py"}

DELAY=5
DEFER=0

################################################################

_msg() { >&2 echo "$*"; }
alert() { _msg "::" "$*"; }
mplog() { _msg "||" "$*"; }
fatal() { alert "$*"; exit 1; }

usage() {
    _msg "usage: $0"
    _msg "  -s <path>   path to sources file (default \"$FLE_SOURCES\")"
    _msg "  -t <path>   path to tracker file (default \"$FLE_TRACKER\")"
    _msg "  -v <level>  log level for $SCR_CONTROL (default \"$LOG_VERBOSE\")"
    _msg
    _msg "  -c          hide captions (subtitles, OSD)"
    _msg "  -d          allow framedropping"
    _msg "  -f          play movies in fullscreen"
    _msg "  -i          show indicator symbol when processing commands"
    _msg "  -m          mute the sound"
    _msg "  -o          keep window on top"
    _msg "  -q          quit on double click"
    _msg
    _msg "  -h          show this help and quit"
    _msg
    [ -n "$*" ] && fatal "$*"
    exit 0
}

OPT_CAPTS=false
OPT_DROPF=false
OPT_FULLS=false
OPT_INDIC=false
OPT_MUTED=false
OPT_ONTOP=false
OPT_QLICK=false

while getopts ":s:t:v:cdfimoqh" OPT "$@"; do
    case $OPT in
        s)  FLE_SOURCES="$OPTARG" ;;
        t)  FLE_TRACKER="$OPTARG" ;;
        v)  LOG_VERBOSE="$OPTARG" ;;
        c)  OPT_CAPTS=true ;;
        d)  OPT_DROPF=true ;;
        f)  OPT_FULLS=true ;;
        i)  OPT_INDIC=true ;;
        m)  OPT_MUTED=true ;;
        o)  OPT_ONTOP=true ;;
        q)  OPT_QLICK=true ;;
        h)  usage ;;
        :)  usage "-$OPTARG needs an argument" ;;
        \?) usage "invalid option: -$OPTARG" ;;
    esac
done
shift $(( OPTIND - 1 ))


################################################################

is_command() { command -v "$1" > /dev/null; return $?; }
is_running() { pgrep "$1" > /dev/null; return $?; }

delete_fifo() {
    fifo="$1"
    if [ -p "$fifo" ]; then
        alert "deleting old fifo at" "$fifo"
        rm "$fifo"
    fi
}
ensure_fifo() {
    fifo="$1"
    [ -p "$fifo" ] && delete_fifo "$fifo"
    [ -e "$fifo" ] && fatal "something, but not a fifo at" "$fifo"
    alert "creating new fifo at" "$fifo"
    mkfifo -m 0644 "$fifo"
}


playerctl() {
    alert "asking" "$SCR_CONTROL" "to" "$*"
    $CMD_PYTHON3 "$SCR_CONTROL" \
        --sources "$FLE_SOURCES" \
        --tracker "$FLE_TRACKER" \
        --verbosity "$LOG_VERBOSE" \
            "$@"
}

start_player() {
    ensure_fifo "$FFO_RECVING"
    ensure_fifo "$FFO_SENDING"

    alert "launching" "$CMD_MPLAYER" "in background" "..."
    alert "..." ">>>" "$FFO_SENDING"
    alert "..." "<<<" "$FFO_RECVING"
    $CMD_MPLAYER \
        -idle \
        -slave \
        -input default-bindings \
        -input file="$FFO_SENDING" \
        -quiet \
        -msgmodule \
        -osdlevel 1 \
        -fontconfig \
        -fixed-vo \
            >> "$FFO_RECVING" \
                2>&1 &
}

send_command() {
    is_running "$CMD_MPLAYER" && \
    [ -p "$FFO_SENDING" ] && \
        echo "$*" >> "$FFO_SENDING"
}

configure() {
    $OPT_CAPTS && {
        send_command "set_property" "sub_visibility" "0"
        send_command "set_property" "osdlevel" "0"
    }
    $OPT_DROPF && send_command "set_property" "framedropping" "1"
    $OPT_FULLS && send_command "set_property" "fullscreen" "1"
    $OPT_MUTED && {
        # This is weird - Need to unmute first, otherwise sound does
        # not get muted reliably.
        send_command "mute" "0"
        send_command "mute" "1"
    }
    $OPT_ONTOP && send_command "set_property" "ontop" "1"
}

play_next() {
    # It takes a while for mplayer to load and play new movies.
    # The request what is currently playing is sent periodically,
    # so we need to make sure not to load multiple movies at once.
    # That is what the defer and delay is for...
    now=$(date +%s)
    if [ "$((DEFER+DELAY))" -le "$now" ]; then
        location=$(playerctl "suggest")
        [ -z "$location" ] && fatal "nothing to play"
        alert "loading" "$location"
        send_command "loadfile" "$location"
        DEFER=$now
        configure
    fi
}

cleanup() {
    if is_running "$CMD_MPLAYER"; then
        pkill "$CMD_MPLAYER" > /dev/null 2>&1
    fi
    delete_fifo "$FFO_RECVING"
    delete_fifo "$FFO_SENDING"
}


################################################################

! is_command "$CMD_MPLAYER" && alert "no such mplayer" "$CMD_MPLAYER"
! is_command "$CMD_PYTHON3" && alert "no such python3" "$CMD_PYTHON3"

if [ -n "$*" ]; then
    playerctl "$@"
    exit $?
fi

playerctl "scan" || fatal "something is wrong"

start_player

trap cleanup 2

while read -r line; do
    case $line in
        *'ANS_path=(null)')
            play_next
            ;;
        *'ANS_path'*) : ;;
        *'MOUSE_BTN0_DBL'*)
            $OPT_QLICK && send_command "quit"
            ;;
        *)
            mplog "$line"
            send_command "get_property" "path"
            ;;
    esac

    $OPT_INDIC && send_command "osd_show_text" "◦"
done < "$FFO_RECVING"

cleanup
exit 0
